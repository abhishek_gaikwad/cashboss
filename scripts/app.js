function FastClick(e, t) {
        "use strict";

        function i(e, t) {
            return function() {
                return e.apply(t, arguments)
            }
        }
        var n;
        if (t = t || {}, this.trackingClick = !1, this.trackingClickStart = 0, this.targetElement = null, this.touchStartX = 0, this.touchStartY = 0, this.lastTouchIdentifier = 0, this.touchBoundary = t.touchBoundary || 10, this.layer = e, this.tapDelay = t.tapDelay || 200, !FastClick.notNeeded(e)) {
            for (var o = ["onMouse", "onClick", "onTouchStart", "onTouchMove", "onTouchEnd", "onTouchCancel"], a = this, r = 0, s = o.length; s > r; r++) a[o[r]] = i(a[o[r]], a);
            deviceIsAndroid && (e.addEventListener("mouseover", this.onMouse, !0), e.addEventListener("mousedown", this.onMouse, !0), e.addEventListener("mouseup", this.onMouse, !0)), e.addEventListener("click", this.onClick, !0), e.addEventListener("touchstart", this.onTouchStart, !1), e.addEventListener("touchmove", this.onTouchMove, !1), e.addEventListener("touchend", this.onTouchEnd, !1), e.addEventListener("touchcancel", this.onTouchCancel, !1), Event.prototype.stopImmediatePropagation || (e.removeEventListener = function(t, i, n) {
                var o = Node.prototype.removeEventListener;
                "click" === t ? o.call(e, t, i.hijacked || i, n) : o.call(e, t, i, n)
            }, e.addEventListener = function(t, i, n) {
                var o = Node.prototype.addEventListener;
                "click" === t ? o.call(e, t, i.hijacked || (i.hijacked = function(e) {
                    e.propagationStopped || i(e)
                }), n) : o.call(e, t, i, n)
            }), "function" == typeof e.onclick && (n = e.onclick, e.addEventListener("click", function(e) {
                n(e)
            }, !1), e.onclick = null)
        }
    }! function(e) {
        var t = {
            sectionContainer: "section",
            easing: "ease",
            animationTime: 1e3,
            pagination: !0,
            updateURL: !1,
            keyboard: !0,
            beforeMove: null,
            afterMove: null,
            loop: !0,
            responsiveFallback: !1,
            direction: "vertical"
        };
        e.fn.swipeEvents = function() {
            return this.each(function() {
                function t(e) {
                    var t = e.originalEvent.touches;
                    t && t.length && (n = t[0].pageX, o = t[0].pageY, a.bind("touchmove", i))
                }

                function i(e) {
                    var t = e.originalEvent.touches;
                    if (t && t.length) {
                        var r = n - t[0].pageX,
                            s = o - t[0].pageY;
                        r >= 50 && a.trigger("swipeLeft"), -50 >= r && a.trigger("swipeRight"), s >= 50 && a.trigger("swipeUp"), -50 >= s && a.trigger("swipeDown"), (Math.abs(r) >= 50 || Math.abs(s) >= 50) && a.unbind("touchmove", i)
                    }
                }
                var n, o, a = e(this);
                a.bind("touchstart", t)
            })
        }, e.fn.onepage_scroll = function(i) {
            function n() {
                var t = !1,
                    i = typeof a.responsiveFallback;
                "number" == i && (t = e(window).width() < a.responsiveFallback), "boolean" == i && (t = a.responsiveFallback), "function" == i && (valFunction = a.responsiveFallback(), t = valFunction, typeOFv = typeof t, "number" == typeOFv && (t = e(window).width() < valFunction)), t ? (e("body").addClass("disabled-onepage-scroll"), e(document).unbind("mousewheel DOMMouseScroll MozMousePixelScroll"), r.swipeEvents().unbind("swipeDown swipeUp")) : (e("body").hasClass("disabled-onepage-scroll") && (e("body").removeClass("disabled-onepage-scroll"), e("html, body, .wrapper").animate({
                    scrollTop: 0
                }, "fast")), r.swipeEvents().bind("swipeDown", function(t) {
                    e("body").hasClass("disabled-onepage-scroll") || t.preventDefault(), r.moveUp()
                }).bind("swipeUp", function(t) {
                    e("body").hasClass("disabled-onepage-scroll") || t.preventDefault(), r.moveDown()
                }), e(document).bind("mousewheel DOMMouseScroll MozMousePixelScroll", function(e) {
                    e.preventDefault();
                    var t = e.originalEvent.wheelDelta || -e.originalEvent.detail;
                    o(e, t)
                }))
            }

            function o(e, t) {
                deltaOfInterest = t;
                var i = (new Date).getTime();
                return i - lastAnimation < quietPeriod + a.animationTime ? void e.preventDefault() : (deltaOfInterest < 0 ? r.moveDown() : r.moveUp(), void(lastAnimation = i))
            }
            var a = e.extend({}, t, i),
                r = e(this),
                s = e(a.sectionContainer);
            if (total = s.length, status = "off", topPos = 0, leftPos = 0, lastAnimation = 0, quietPeriod = 500, paginationList = "", e.fn.transformPage = function(t, i, n) {
                    if ("function" == typeof t.beforeMove && t.beforeMove(n), e("html").hasClass("ie8"))
                        if ("horizontal" == t.direction) {
                            var o = r.width() / 100 * i;
                            e(this).animate({
                                left: o + "px"
                            }, t.animationTime)
                        } else {
                            var o = r.height() / 100 * i;
                            e(this).animate({
                                top: o + "px"
                            }, t.animationTime)
                        } else e(this).css({
                        "-webkit-transform": "horizontal" == t.direction ? "translate3d(" + i + "%, 0, 0)" : "translate3d(0, " + i + "%, 0)",
                        "-webkit-transition": "all " + t.animationTime + "ms " + t.easing,
                        "-moz-transform": "horizontal" == t.direction ? "translate3d(" + i + "%, 0, 0)" : "translate3d(0, " + i + "%, 0)",
                        "-moz-transition": "all " + t.animationTime + "ms " + t.easing,
                        "-ms-transform": "horizontal" == t.direction ? "translate3d(" + i + "%, 0, 0)" : "translate3d(0, " + i + "%, 0)",
                        "-ms-transition": "all " + t.animationTime + "ms " + t.easing,
                        transform: "horizontal" == t.direction ? "translate3d(" + i + "%, 0, 0)" : "translate3d(0, " + i + "%, 0)",
                        transition: "all " + t.animationTime + "ms " + t.easing
                    });
                    e(this).one("webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend", function() {
                        "function" == typeof t.afterMove && t.afterMove(n)
                    })
                }, e.fn.moveDown = function() {
                    var t = e(this);
                    if (index = e(a.sectionContainer + ".active").data("index"), current = e(a.sectionContainer + "[data-index='" + index + "']"), next = e(a.sectionContainer + "[data-index='" + (index + 1) + "']"), next.length < 1) {
                        if (1 != a.loop) return;
                        pos = 0, next = e(a.sectionContainer + "[data-index='1']")
                    } else pos = 100 * index * -1;
                    if ("function" == typeof a.beforeMove && a.beforeMove(next.data("index")), current.removeClass("active"), next.addClass("active"), 1 == a.pagination && (e(".onepage-pagination li a[data-index='" + index + "']").removeClass("active"), e(".onepage-pagination li a[data-index='" + next.data("index") + "']").addClass("active")), e("body")[0].className = e("body")[0].className.replace(/\bviewing-page-\d.*?\b/g, ""), e("body").addClass("viewing-page-" + next.data("index")), history.replaceState && 1 == a.updateURL) {
                        var i = window.location.href.substr(0, window.location.href.indexOf("#")) + "#" + (index + 1);
                        history.pushState({}, document.title, i)
                    }
                    t.transformPage(a, pos, next.data("index"))
                }, e.fn.moveUp = function() {
                    var t = e(this);
                    if (index = e(a.sectionContainer + ".active").data("index"), current = e(a.sectionContainer + "[data-index='" + index + "']"), next = e(a.sectionContainer + "[data-index='" + (index - 1) + "']"), next.length < 1) {
                        if (1 != a.loop) return;
                        pos = 100 * (total - 1) * -1, next = e(a.sectionContainer + "[data-index='" + total + "']")
                    } else pos = 100 * (next.data("index") - 1) * -1;
                    if ("function" == typeof a.beforeMove && a.beforeMove(next.data("index")), current.removeClass("active"), next.addClass("active"), 1 == a.pagination && (e(".onepage-pagination li a[data-index='" + index + "']").removeClass("active"), e(".onepage-pagination li a[data-index='" + next.data("index") + "']").addClass("active")), e("body")[0].className = e("body")[0].className.replace(/\bviewing-page-\d.*?\b/g, ""), e("body").addClass("viewing-page-" + next.data("index")), history.replaceState && 1 == a.updateURL) {
                        var i = window.location.href.substr(0, window.location.href.indexOf("#")) + "#" + (index - 1);
                        history.pushState({}, document.title, i)
                    }
                    t.transformPage(a, pos, next.data("index"))
                }, e.fn.moveTo = function(t) {
                    if (current = e(a.sectionContainer + ".active"), next = e(a.sectionContainer + "[data-index='" + t + "']"), next.length > 0) {
                        if ("function" == typeof a.beforeMove && a.beforeMove(next.data("index")), current.removeClass("active"), next.addClass("active"), e(".onepage-pagination li a.active").removeClass("active"), e(".onepage-pagination li a[data-index='" + t + "']").addClass("active"), e("body")[0].className = e("body")[0].className.replace(/\bviewing-page-\d.*?\b/g, ""), e("body").addClass("viewing-page-" + next.data("index")), pos = 100 * (t - 1) * -1, history.replaceState && 1 == a.updateURL) {
                            var i = window.location.href.substr(0, window.location.href.indexOf("#")) + "#" + (t - 1);
                            history.pushState({}, document.title, i)
                        }
                        r.transformPage(a, pos, t)
                    }
                }, r.addClass("onepage-wrapper").css("position", "relative"), e.each(s, function(t) {
                    e(this).css({
                        position: "absolute",
                        top: topPos + "%"
                    }).addClass("section").attr("data-index", t + 1), e(this).css({
                        position: "absolute",
                        left: "horizontal" == a.direction ? leftPos + "%" : 0,
                        top: "vertical" == a.direction || "horizontal" != a.direction ? topPos + "%" : 0
                    }), "horizontal" == a.direction ? leftPos += 100 : topPos += 100, 1 == a.pagination && (paginationList += "<li><a data-index='" + (t + 1) + "' href='#" + (t + 1) + "'></a></li>")
                }), r.swipeEvents().bind("swipeDown", function(t) {
                    e("body").hasClass("disabled-onepage-scroll") || t.preventDefault(), r.moveUp()
                }).bind("swipeUp", function(t) {
                    e("body").hasClass("disabled-onepage-scroll") || t.preventDefault(), r.moveDown()
                }), 1 == a.pagination && (e("ul.onepage-pagination").length < 1 && e("<ul class='onepage-pagination'></ul>").prependTo("body"), "horizontal" == a.direction ? (posLeft = r.find(".onepage-pagination").width() / 2 * -1, r.find(".onepage-pagination").css("margin-left", posLeft)) : (posTop = r.find(".onepage-pagination").height() / 2 * -1, r.find(".onepage-pagination").css("margin-top", posTop)), e("ul.onepage-pagination").html(paginationList)), "" != window.location.hash && "#1" != window.location.hash)
                if (init_index = window.location.hash.replace("#", ""), parseInt(init_index) <= total && parseInt(init_index) > 0) {
                    if (e(a.sectionContainer + "[data-index='" + init_index + "']").addClass("active"), e("body").addClass("viewing-page-" + init_index), 1 == a.pagination && e(".onepage-pagination li a[data-index='" + init_index + "']").addClass("active"), next = e(a.sectionContainer + "[data-index='" + init_index + "']"), next && (next.addClass("active"), 1 == a.pagination && e(".onepage-pagination li a[data-index='" + init_index + "']").addClass("active"), e("body")[0].className = e("body")[0].className.replace(/\bviewing-page-\d.*?\b/g, ""), e("body").addClass("viewing-page-" + next.data("index")), history.replaceState && 1 == a.updateURL)) {
                        var c = window.location.href.substr(0, window.location.href.indexOf("#")) + "#" + init_index;
                        history.pushState({}, document.title, c)
                    }
                    pos = 100 * (init_index - 1) * -1, r.transformPage(a, pos, init_index)
                } else e(a.sectionContainer + "[data-index='1']").addClass("active"), e("body").addClass("viewing-page-1"), 1 == a.pagination && e(".onepage-pagination li a[data-index='1']").addClass("active");
            else e(a.sectionContainer + "[data-index='1']").addClass("active"), e("body").addClass("viewing-page-1"), 1 == a.pagination && e(".onepage-pagination li a[data-index='1']").addClass("active");
            return 1 == a.pagination && e(".onepage-pagination li a").click(function() {
                var t = e(this).data("index");
                r.moveTo(t)
            }), e(document).bind("mousewheel DOMMouseScroll MozMousePixelScroll", function(t) {
                t.preventDefault();
                var i = t.originalEvent.wheelDelta || -t.originalEvent.detail;
                e("body").hasClass("disabled-onepage-scroll") || o(t, i)
            }), 0 != a.responsiveFallback && (e(window).resize(function() {
                n()
            }), n()), 1 == a.keyboard && e(document).keydown(function(t) {
                var i = t.target.tagName.toLowerCase();
                if (!e("body").hasClass("disabled-onepage-scroll")) switch (t.which) {
                    case 38:
                        "input" != i && "textarea" != i && r.moveUp();
                        break;
                    case 40:
                        "input" != i && "textarea" != i && r.moveDown();
                        break;
                    case 32:
                        "input" != i && "textarea" != i && r.moveDown();
                        break;
                    case 33:
                        "input" != i && "textarea" != i && r.moveUp();
                        break;
                    case 34:
                        "input" != i && "textarea" != i && r.moveDown();
                        break;
                    case 36:
                        r.moveTo(1);
                        break;
                    case 35:
                        r.moveTo(total);
                        break;
                    default:
                        return
                }
            }), !1
        }
    }(window.jQuery),
    function(e) {
        "function" == typeof define && define.amd ? define(["jquery"], e) : e("object" == typeof exports ? require("jquery") : window.jQuery || window.Zepto)
    }(function(e) {
        var t, i, n, o, a, r, s, c = "Close",
            l = "BeforeClose",
            d = "AfterClose",
            p = "BeforeAppend",
            u = "MarkupParse",
            f = "Open",
            m = "Change",
            g = "mfp",
            h = "." + g,
            v = "mfp-ready",
            w = "mfp-removing",
            b = "mfp-prevent-close",
            y = function() {},
            C = !!window.jQuery,
            k = e(window),
            x = function(e, i) {
                t.ev.on(g + e + h, i)
            },
            T = function(t, i, n, o) {
                var a = document.createElement("div");
                return a.className = "mfp-" + t, n && (a.innerHTML = n), o ? i && i.appendChild(a) : (a = e(a), i && a.appendTo(i)), a
            },
            I = function(i, n) {
                t.ev.triggerHandler(g + i, n), t.st.callbacks && (i = i.charAt(0).toLowerCase() + i.slice(1), t.st.callbacks[i] && t.st.callbacks[i].apply(t, e.isArray(n) ? n : [n]))
            },
            E = function(i) {
                return i === s && t.currTemplate.closeBtn || (t.currTemplate.closeBtn = e(t.st.closeMarkup.replace("%title%", t.st.tClose)), s = i), t.currTemplate.closeBtn
            },
            S = function() {
                e.magnificPopup.instance || (t = new y, t.init(), e.magnificPopup.instance = t)
            },
            M = function() {
                var e = document.createElement("p").style,
                    t = ["ms", "O", "Moz", "Webkit"];
                if (void 0 !== e.transition) return !0;
                for (; t.length;)
                    if (t.pop() + "Transition" in e) return !0;
                return !1
            };
        y.prototype = {
            constructor: y,
            init: function() {
                var i = navigator.appVersion;
                t.isIE7 = -1 !== i.indexOf("MSIE 7."), t.isIE8 = -1 !== i.indexOf("MSIE 8."), t.isLowIE = t.isIE7 || t.isIE8, t.isAndroid = /android/gi.test(i), t.isIOS = /iphone|ipad|ipod/gi.test(i), t.supportsTransition = M(), t.probablyMobile = t.isAndroid || t.isIOS || /(Opera Mini)|Kindle|webOS|BlackBerry|(Opera Mobi)|(Windows Phone)|IEMobile/i.test(navigator.userAgent), o = e(document), t.popupsCache = {}
            },
            open: function(i) {
                n || (n = e(document.body));
                var a;
                if (i.isObj === !1) {
                    t.items = i.items.toArray(), t.index = 0;
                    var s, c = i.items;
                    for (a = 0; a < c.length; a++)
                        if (s = c[a], s.parsed && (s = s.el[0]), s === i.el[0]) {
                            t.index = a;
                            break
                        }
                } else t.items = e.isArray(i.items) ? i.items : [i.items], t.index = i.index || 0;
                if (t.isOpen) return void t.updateItemHTML();
                t.types = [], r = "", t.ev = i.mainEl && i.mainEl.length ? i.mainEl.eq(0) : o, i.key ? (t.popupsCache[i.key] || (t.popupsCache[i.key] = {}), t.currTemplate = t.popupsCache[i.key]) : t.currTemplate = {}, t.st = e.extend(!0, {}, e.magnificPopup.defaults, i), t.fixedContentPos = "auto" === t.st.fixedContentPos ? !t.probablyMobile : t.st.fixedContentPos, t.st.modal && (t.st.closeOnContentClick = !1, t.st.closeOnBgClick = !1, t.st.showCloseBtn = !1, t.st.enableEscapeKey = !1), t.bgOverlay || (t.bgOverlay = T("bg").on("click" + h, function() {
                    t.close()
                }), t.wrap = T("wrap").attr("tabindex", -1).on("click" + h, function(e) {
                    t._checkIfClose(e.target) && t.close()
                }), t.container = T("container", t.wrap)), t.contentContainer = T("content"), t.st.preloader && (t.preloader = T("preloader", t.container, t.st.tLoading));
                var l = e.magnificPopup.modules;
                for (a = 0; a < l.length; a++) {
                    var d = l[a];
                    d = d.charAt(0).toUpperCase() + d.slice(1), t["init" + d].call(t)
                }
                I("BeforeOpen"), t.st.showCloseBtn && (t.st.closeBtnInside ? (x(u, function(e, t, i, n) {
                    i.close_replaceWith = E(n.type)
                }), r += " mfp-close-btn-in") : t.wrap.append(E())), t.st.alignTop && (r += " mfp-align-top"), t.wrap.css(t.fixedContentPos ? {
                    overflow: t.st.overflowY,
                    overflowX: "hidden",
                    overflowY: t.st.overflowY
                } : {
                    top: k.scrollTop(),
                    position: "absolute"
                }), (t.st.fixedBgPos === !1 || "auto" === t.st.fixedBgPos && !t.fixedContentPos) && t.bgOverlay.css({
                    height: o.height(),
                    position: "absolute"
                }), t.st.enableEscapeKey && o.on("keyup" + h, function(e) {
                    27 === e.keyCode && t.close()
                }), k.on("resize" + h, function() {
                    t.updateSize()
                }), t.st.closeOnContentClick || (r += " mfp-auto-cursor"), r && t.wrap.addClass(r);
                var p = t.wH = k.height(),
                    m = {};
                if (t.fixedContentPos && t._hasScrollBar(p)) {
                    var g = t._getScrollbarSize();
                    g && (m.marginRight = g)
                }
                t.fixedContentPos && (t.isIE7 ? e("body, html").css("overflow", "hidden") : m.overflow = "hidden");
                var w = t.st.mainClass;
                return t.isIE7 && (w += " mfp-ie7"), w && t._addClassToMFP(w), t.updateItemHTML(), I("BuildControls"), e("html").css(m), t.bgOverlay.add(t.wrap).prependTo(t.st.prependTo || n), t._lastFocusedEl = document.activeElement, setTimeout(function() {
                    t.content ? (t._addClassToMFP(v), t._setFocus()) : t.bgOverlay.addClass(v), o.on("focusin" + h, t._onFocusIn)
                }, 16), t.isOpen = !0, t.updateSize(p), I(f), i
            },
            close: function() {
                t.isOpen && (I(l), t.isOpen = !1, t.st.removalDelay && !t.isLowIE && t.supportsTransition ? (t._addClassToMFP(w), setTimeout(function() {
                    t._close()
                }, t.st.removalDelay)) : t._close())
            },
            _close: function() {
                I(c);
                var i = w + " " + v + " ";
                if (t.bgOverlay.detach(), t.wrap.detach(), t.container.empty(), t.st.mainClass && (i += t.st.mainClass + " "), t._removeClassFromMFP(i), t.fixedContentPos) {
                    var n = {
                        marginRight: ""
                    };
                    t.isIE7 ? e("body, html").css("overflow", "") : n.overflow = "", e("html").css(n)
                }
                o.off("keyup" + h + " focusin" + h), t.ev.off(h), t.wrap.attr("class", "mfp-wrap").removeAttr("style"), t.bgOverlay.attr("class", "mfp-bg"), t.container.attr("class", "mfp-container"), !t.st.showCloseBtn || t.st.closeBtnInside && t.currTemplate[t.currItem.type] !== !0 || t.currTemplate.closeBtn && t.currTemplate.closeBtn.detach(), t._lastFocusedEl && e(t._lastFocusedEl).focus(), t.currItem = null, t.content = null, t.currTemplate = null, t.prevHeight = 0, I(d)
            },
            updateSize: function(e) {
                if (t.isIOS) {
                    var i = document.documentElement.clientWidth / window.innerWidth,
                        n = window.innerHeight * i;
                    t.wrap.css("height", n), t.wH = n
                } else t.wH = e || k.height();
                t.fixedContentPos || t.wrap.css("height", t.wH), I("Resize")
            },
            updateItemHTML: function() {
                var i = t.items[t.index];
                t.contentContainer.detach(), t.content && t.content.detach(), i.parsed || (i = t.parseEl(t.index));
                var n = i.type;
                if (I("BeforeChange", [t.currItem ? t.currItem.type : "", n]), t.currItem = i, !t.currTemplate[n]) {
                    var o = t.st[n] ? t.st[n].markup : !1;
                    I("FirstMarkupParse", o), t.currTemplate[n] = o ? e(o) : !0
                }
                a && a !== i.type && t.container.removeClass("mfp-" + a + "-holder");
                var r = t["get" + n.charAt(0).toUpperCase() + n.slice(1)](i, t.currTemplate[n]);
                t.appendContent(r, n), i.preloaded = !0, I(m, i), a = i.type, t.container.prepend(t.contentContainer), I("AfterChange")
            },
            appendContent: function(e, i) {
                t.content = e, e ? t.st.showCloseBtn && t.st.closeBtnInside && t.currTemplate[i] === !0 ? t.content.find(".mfp-close").length || t.content.append(E()) : t.content = e : t.content = "", I(p), t.container.addClass("mfp-" + i + "-holder"), t.contentContainer.append(t.content)
            },
            parseEl: function(i) {
                var n, o = t.items[i];
                if (o.tagName ? o = {
                        el: e(o)
                    } : (n = o.type, o = {
                        data: o,
                        src: o.src
                    }), o.el) {
                    for (var a = t.types, r = 0; r < a.length; r++)
                        if (o.el.hasClass("mfp-" + a[r])) {
                            n = a[r];
                            break
                        }
                    o.src = o.el.attr("data-mfp-src"), o.src || (o.src = o.el.attr("href"))
                }
                return o.type = n || t.st.type || "inline", o.index = i, o.parsed = !0, t.items[i] = o, I("ElementParse", o), t.items[i]
            },
            addGroup: function(e, i) {
                var n = function(n) {
                    n.mfpEl = this, t._openClick(n, e, i)
                };
                i || (i = {});
                var o = "click.magnificPopup";
                i.mainEl = e, i.items ? (i.isObj = !0, e.off(o).on(o, n)) : (i.isObj = !1, i.delegate ? e.off(o).on(o, i.delegate, n) : (i.items = e, e.off(o).on(o, n)))
            },
            _openClick: function(i, n, o) {
                var a = void 0 !== o.midClick ? o.midClick : e.magnificPopup.defaults.midClick;
                if (a || 2 !== i.which && !i.ctrlKey && !i.metaKey) {
                    var r = void 0 !== o.disableOn ? o.disableOn : e.magnificPopup.defaults.disableOn;
                    if (r)
                        if (e.isFunction(r)) {
                            if (!r.call(t)) return !0
                        } else if (k.width() < r) return !0;
                    i.type && (i.preventDefault(), t.isOpen && i.stopPropagation()), o.el = e(i.mfpEl), o.delegate && (o.items = n.find(o.delegate)), t.open(o)
                }
            },
            updateStatus: function(e, n) {
                if (t.preloader) {
                    i !== e && t.container.removeClass("mfp-s-" + i), n || "loading" !== e || (n = t.st.tLoading);
                    var o = {
                        status: e,
                        text: n
                    };
                    I("UpdateStatus", o), e = o.status, n = o.text, t.preloader.html(n), t.preloader.find("a").on("click", function(e) {
                        e.stopImmediatePropagation()
                    }), t.container.addClass("mfp-s-" + e), i = e
                }
            },
            _checkIfClose: function(i) {
                if (!e(i).hasClass(b)) {
                    var n = t.st.closeOnContentClick,
                        o = t.st.closeOnBgClick;
                    if (n && o) return !0;
                    if (!t.content || e(i).hasClass("mfp-close") || t.preloader && i === t.preloader[0]) return !0;
                    if (i === t.content[0] || e.contains(t.content[0], i)) {
                        if (n) return !0
                    } else if (o && e.contains(document, i)) return !0;
                    return !1
                }
            },
            _addClassToMFP: function(e) {
                t.bgOverlay.addClass(e), t.wrap.addClass(e)
            },
            _removeClassFromMFP: function(e) {
                this.bgOverlay.removeClass(e), t.wrap.removeClass(e)
            },
            _hasScrollBar: function(e) {
                return (t.isIE7 ? o.height() : document.body.scrollHeight) > (e || k.height())
            },
            _setFocus: function() {
                (t.st.focus ? t.content.find(t.st.focus).eq(0) : t.wrap).focus()
            },
            _onFocusIn: function(i) {
                return i.target === t.wrap[0] || e.contains(t.wrap[0], i.target) ? void 0 : (t._setFocus(), !1)
            },
            _parseMarkup: function(t, i, n) {
                var o;
                n.data && (i = e.extend(n.data, i)), I(u, [t, i, n]), e.each(i, function(e, i) {
                    if (void 0 === i || i === !1) return !0;
                    if (o = e.split("_"), o.length > 1) {
                        var n = t.find(h + "-" + o[0]);
                        if (n.length > 0) {
                            var a = o[1];
                            "replaceWith" === a ? n[0] !== i[0] && n.replaceWith(i) : "img" === a ? n.is("img") ? n.attr("src", i) : n.replaceWith('<img src="' + i + '" class="' + n.attr("class") + '" />') : n.attr(o[1], i)
                        }
                    } else t.find(h + "-" + e).html(i)
                })
            },
            _getScrollbarSize: function() {
                if (void 0 === t.scrollbarSize) {
                    var e = document.createElement("div");
                    e.style.cssText = "width: 99px; height: 99px; overflow: scroll; position: absolute; top: -9999px;", document.body.appendChild(e), t.scrollbarSize = e.offsetWidth - e.clientWidth, document.body.removeChild(e)
                }
                return t.scrollbarSize
            }
        }, e.magnificPopup = {
            instance: null,
            proto: y.prototype,
            modules: [],
            open: function(t, i) {
                return S(), t = t ? e.extend(!0, {}, t) : {}, t.isObj = !0, t.index = i || 0, this.instance.open(t)
            },
            close: function() {
                return e.magnificPopup.instance && e.magnificPopup.instance.close()
            },
            registerModule: function(t, i) {
                i.options && (e.magnificPopup.defaults[t] = i.options), e.extend(this.proto, i.proto), this.modules.push(t)
            },
            defaults: {
                disableOn: 0,
                key: null,
                midClick: !1,
                mainClass: "",
                preloader: !0,
                focus: "",
                closeOnContentClick: !1,
                closeOnBgClick: !0,
                closeBtnInside: !0,
                showCloseBtn: !0,
                enableEscapeKey: !0,
                modal: !1,
                alignTop: !1,
                removalDelay: 0,
                prependTo: null,
                fixedContentPos: "auto",
                fixedBgPos: "auto",
                overflowY: "auto",
                closeMarkup: '<button title="%title%" type="button" class="mfp-close">&times;</button>',
                tClose: "Close (Esc)",
                tLoading: "Loading..."
            }
        }, e.fn.magnificPopup = function(i) {
            S();
            var n = e(this);
            if ("string" == typeof i)
                if ("open" === i) {
                    var o, a = C ? n.data("magnificPopup") : n[0].magnificPopup,
                        r = parseInt(arguments[1], 10) || 0;
                    a.items ? o = a.items[r] : (o = n, a.delegate && (o = o.find(a.delegate)), o = o.eq(r)), t._openClick({
                        mfpEl: o
                    }, n, a)
                } else t.isOpen && t[i].apply(t, Array.prototype.slice.call(arguments, 1));
            else i = e.extend(!0, {}, i), C ? n.data("magnificPopup", i) : n[0].magnificPopup = i, t.addGroup(n, i);
            return n
        };
        var O, P, _, z = "inline",
            F = function() {
                _ && (P.after(_.addClass(O)).detach(), _ = null)
            };
        e.magnificPopup.registerModule(z, {
            options: {
                hiddenClass: "hide",
                markup: "",
                tNotFound: "Content not found"
            },
            proto: {
                initInline: function() {
                    t.types.push(z), x(c + "." + z, function() {
                        F()
                    })
                },
                getInline: function(i, n) {
                    if (F(), i.src) {
                        var o = t.st.inline,
                            a = e(i.src);
                        if (a.length) {
                            var r = a[0].parentNode;
                            r && r.tagName && (P || (O = o.hiddenClass, P = T(O), O = "mfp-" + O), _ = a.after(P).detach().removeClass(O)), t.updateStatus("ready")
                        } else t.updateStatus("error", o.tNotFound), a = e("<div>");
                        return i.inlineElement = a, a
                    }
                    return t.updateStatus("ready"), t._parseMarkup(n, {}, i), n
                }
            }
        });
        var L, $ = "ajax",
            A = function() {
                L && n.removeClass(L)
            },
            B = function() {
                A(), t.req && t.req.abort()
            };
        e.magnificPopup.registerModule($, {
            options: {
                settings: null,
                cursor: "mfp-ajax-cur",
                tError: '<a href="%url%">The content</a> could not be loaded.'
            },
            proto: {
                initAjax: function() {
                    t.types.push($), L = t.st.ajax.cursor, x(c + "." + $, B), x("BeforeChange." + $, B)
                },
                getAjax: function(i) {
                    L && n.addClass(L), t.updateStatus("loading");
                    var o = e.extend({
                        url: i.src,
                        success: function(n, o, a) {
                            var r = {
                                data: n,
                                xhr: a
                            };
                            I("ParseAjax", r), t.appendContent(e(r.data), $), i.finished = !0, A(), t._setFocus(), setTimeout(function() {
                                t.wrap.addClass(v)
                            }, 16), t.updateStatus("ready"), I("AjaxContentAdded")
                        },
                        error: function() {
                            A(), i.finished = i.loadError = !0, t.updateStatus("error", t.st.ajax.tError.replace("%url%", i.src))
                        }
                    }, t.st.ajax.settings);
                    return t.req = e.ajax(o), ""
                }
            }
        });
        var D, N = function(i) {
            if (i.data && void 0 !== i.data.title) return i.data.title;
            var n = t.st.image.titleSrc;
            if (n) {
                if (e.isFunction(n)) return n.call(t, i);
                if (i.el) return i.el.attr(n) || ""
            }
            return ""
        };
        e.magnificPopup.registerModule("image", {
            options: {
                markup: '<div class="mfp-figure"><div class="mfp-close"></div><figure><div class="mfp-img"></div><figcaption><div class="mfp-bottom-bar"><div class="mfp-title"></div><div class="mfp-counter"></div></div></figcaption></figure></div>',
                cursor: "mfp-zoom-out-cur",
                titleSrc: "title",
                verticalFit: !0,
                tError: '<a href="%url%">The image</a> could not be loaded.'
            },
            proto: {
                initImage: function() {
                    var e = t.st.image,
                        i = ".image";
                    t.types.push("image"), x(f + i, function() {
                        "image" === t.currItem.type && e.cursor && n.addClass(e.cursor)
                    }), x(c + i, function() {
                        e.cursor && n.removeClass(e.cursor), k.off("resize" + h)
                    }), x("Resize" + i, t.resizeImage), t.isLowIE && x("AfterChange", t.resizeImage)
                },
                resizeImage: function() {
                    var e = t.currItem;
                    if (e && e.img && t.st.image.verticalFit) {
                        var i = 0;
                        t.isLowIE && (i = parseInt(e.img.css("padding-top"), 10) + parseInt(e.img.css("padding-bottom"), 10)), e.img.css("max-height", t.wH - i)
                    }
                },
                _onImageHasSize: function(e) {
                    e.img && (e.hasSize = !0, D && clearInterval(D), e.isCheckingImgSize = !1, I("ImageHasSize", e), e.imgHidden && (t.content && t.content.removeClass("mfp-loading"), e.imgHidden = !1))
                },
                findImageSize: function(e) {
                    var i = 0,
                        n = e.img[0],
                        o = function(a) {
                            D && clearInterval(D), D = setInterval(function() {
                                return n.naturalWidth > 0 ? void t._onImageHasSize(e) : (i > 200 && clearInterval(D), i++, void(3 === i ? o(10) : 40 === i ? o(50) : 100 === i && o(500)))
                            }, a)
                        };
                    o(1)
                },
                getImage: function(i, n) {
                    var o = 0,
                        a = function() {
                            i && (i.img[0].complete ? (i.img.off(".mfploader"), i === t.currItem && (t._onImageHasSize(i), t.updateStatus("ready")), i.hasSize = !0, i.loaded = !0, I("ImageLoadComplete")) : (o++, 200 > o ? setTimeout(a, 100) : r()))
                        },
                        r = function() {
                            i && (i.img.off(".mfploader"), i === t.currItem && (t._onImageHasSize(i), t.updateStatus("error", s.tError.replace("%url%", i.src))), i.hasSize = !0, i.loaded = !0, i.loadError = !0)
                        },
                        s = t.st.image,
                        c = n.find(".mfp-img");
                    if (c.length) {
                        var l = document.createElement("img");
                        l.className = "mfp-img", i.el && i.el.find("img").length && (l.alt = i.el.find("img").attr("alt")), i.img = e(l).on("load.mfploader", a).on("error.mfploader", r), l.src = i.src, c.is("img") && (i.img = i.img.clone()), l = i.img[0], l.naturalWidth > 0 ? i.hasSize = !0 : l.width || (i.hasSize = !1)
                    }
                    return t._parseMarkup(n, {
                        title: N(i),
                        img_replaceWith: i.img
                    }, i), t.resizeImage(), i.hasSize ? (D && clearInterval(D), i.loadError ? (n.addClass("mfp-loading"), t.updateStatus("error", s.tError.replace("%url%", i.src))) : (n.removeClass("mfp-loading"), t.updateStatus("ready")), n) : (t.updateStatus("loading"), i.loading = !0, i.hasSize || (i.imgHidden = !0, n.addClass("mfp-loading"), t.findImageSize(i)), n)
                }
            }
        });
        var j, H = function() {
            return void 0 === j && (j = void 0 !== document.createElement("p").style.MozTransform), j
        };
        e.magnificPopup.registerModule("zoom", {
            options: {
                enabled: !1,
                easing: "ease-in-out",
                duration: 300,
                opener: function(e) {
                    return e.is("img") ? e : e.find("img")
                }
            },
            proto: {
                initZoom: function() {
                    var e, i = t.st.zoom,
                        n = ".zoom";
                    if (i.enabled && t.supportsTransition) {
                        var o, a, r = i.duration,
                            s = function(e) {
                                var t = e.clone().removeAttr("style").removeAttr("class").addClass("mfp-animated-image"),
                                    n = "all " + i.duration / 1e3 + "s " + i.easing,
                                    o = {
                                        position: "fixed",
                                        zIndex: 9999,
                                        left: 0,
                                        top: 0,
                                        "-webkit-backface-visibility": "hidden"
                                    },
                                    a = "transition";
                                return o["-webkit-" + a] = o["-moz-" + a] = o["-o-" + a] = o[a] = n, t.css(o), t
                            },
                            d = function() {
                                t.content.css("visibility", "visible")
                            };
                        x("BuildControls" + n, function() {
                            if (t._allowZoom()) {
                                if (clearTimeout(o), t.content.css("visibility", "hidden"), e = t._getItemToZoom(), !e) return void d();
                                a = s(e), a.css(t._getOffset()), t.wrap.append(a), o = setTimeout(function() {
                                    a.css(t._getOffset(!0)), o = setTimeout(function() {
                                        d(), setTimeout(function() {
                                            a.remove(), e = a = null, I("ZoomAnimationEnded")
                                        }, 16)
                                    }, r)
                                }, 16)
                            }
                        }), x(l + n, function() {
                            if (t._allowZoom()) {
                                if (clearTimeout(o), t.st.removalDelay = r, !e) {
                                    if (e = t._getItemToZoom(), !e) return;
                                    a = s(e)
                                }
                                a.css(t._getOffset(!0)), t.wrap.append(a), t.content.css("visibility", "hidden"), setTimeout(function() {
                                    a.css(t._getOffset())
                                }, 16)
                            }
                        }), x(c + n, function() {
                            t._allowZoom() && (d(), a && a.remove(), e = null)
                        })
                    }
                },
                _allowZoom: function() {
                    return "image" === t.currItem.type
                },
                _getItemToZoom: function() {
                    return t.currItem.hasSize ? t.currItem.img : !1
                },
                _getOffset: function(i) {
                    var n;
                    n = i ? t.currItem.img : t.st.zoom.opener(t.currItem.el || t.currItem);
                    var o = n.offset(),
                        a = parseInt(n.css("padding-top"), 10),
                        r = parseInt(n.css("padding-bottom"), 10);
                    o.top -= e(window).scrollTop() - a;
                    var s = {
                        width: n.width(),
                        height: (C ? n.innerHeight() : n[0].offsetHeight) - r - a
                    };
                    return H() ? s["-moz-transform"] = s.transform = "translate(" + o.left + "px," + o.top + "px)" : (s.left = o.left, s.top = o.top), s
                }
            }
        });
        var R = "iframe",
            W = "//about:blank",
            U = function(e) {
                if (t.currTemplate[R]) {
                    var i = t.currTemplate[R].find("iframe");
                    i.length && (e || (i[0].src = W), t.isIE8 && i.css("display", e ? "block" : "none"))
                }
            };
        e.magnificPopup.registerModule(R, {
            options: {
                markup: '<div class="mfp-iframe-scaler"><div class="mfp-close"></div><iframe class="mfp-iframe" src="//about:blank" frameborder="0" allowfullscreen></iframe></div>',
                srcAction: "iframe_src",
                patterns: {
                    youtube: {
                        index: "youtube.com",
                        id: "v=",
                        src: "//www.youtube.com/embed/%id%?autoplay=1"
                    },
                    vimeo: {
                        index: "vimeo.com/",
                        id: "/",
                        src: "//player.vimeo.com/video/%id%?autoplay=1"
                    },
                    gmaps: {
                        index: "//maps.google.",
                        src: "%id%&output=embed"
                    }
                }
            },
            proto: {
                initIframe: function() {
                    t.types.push(R), x("BeforeChange", function(e, t, i) {
                        t !== i && (t === R ? U() : i === R && U(!0))
                    }), x(c + "." + R, function() {
                        U()
                    })
                },
                getIframe: function(i, n) {
                    var o = i.src,
                        a = t.st.iframe;
                    e.each(a.patterns, function() {
                        return o.indexOf(this.index) > -1 ? (this.id && (o = "string" == typeof this.id ? o.substr(o.lastIndexOf(this.id) + this.id.length, o.length) : this.id.call(this, o)), o = this.src.replace("%id%", o), !1) : void 0
                    });
                    var r = {};
                    return a.srcAction && (r[a.srcAction] = o), t._parseMarkup(n, r, i), t.updateStatus("ready"), n
                }
            }
        });
        var Y = function(e) {
                var i = t.items.length;
                return e > i - 1 ? e - i : 0 > e ? i + e : e
            },
            X = function(e, t, i) {
                return e.replace(/%curr%/gi, t + 1).replace(/%total%/gi, i)
            };
        e.magnificPopup.registerModule("gallery", {
            options: {
                enabled: !1,
                arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>',
                preload: [0, 2],
                navigateByImgClick: !0,
                arrows: !0,
                tPrev: "Previous (Left arrow key)",
                tNext: "Next (Right arrow key)",
                tCounter: "%curr% of %total%"
            },
            proto: {
                initGallery: function() {
                    var i = t.st.gallery,
                        n = ".mfp-gallery",
                        a = Boolean(e.fn.mfpFastClick);
                    return t.direction = !0, i && i.enabled ? (r += " mfp-gallery", x(f + n, function() {
                        i.navigateByImgClick && t.wrap.on("click" + n, ".mfp-img", function() {
                            return t.items.length > 1 ? (t.next(), !1) : void 0
                        }), o.on("keydown" + n, function(e) {
                            37 === e.keyCode ? t.prev() : 39 === e.keyCode && t.next()
                        })
                    }), x("UpdateStatus" + n, function(e, i) {
                        i.text && (i.text = X(i.text, t.currItem.index, t.items.length))
                    }), x(u + n, function(e, n, o, a) {
                        var r = t.items.length;
                        o.counter = r > 1 ? X(i.tCounter, a.index, r) : ""
                    }), x("BuildControls" + n, function() {
                        if (t.items.length > 1 && i.arrows && !t.arrowLeft) {
                            var n = i.arrowMarkup,
                                o = t.arrowLeft = e(n.replace(/%title%/gi, i.tPrev).replace(/%dir%/gi, "left")).addClass(b),
                                r = t.arrowRight = e(n.replace(/%title%/gi, i.tNext).replace(/%dir%/gi, "right")).addClass(b),
                                s = a ? "mfpFastClick" : "click";
                            o[s](function() {
                                t.prev()
                            }), r[s](function() {
                                t.next()
                            }), t.isIE7 && (T("b", o[0], !1, !0), T("a", o[0], !1, !0), T("b", r[0], !1, !0), T("a", r[0], !1, !0)), t.container.append(o.add(r))
                        }
                    }), x(m + n, function() {
                        t._preloadTimeout && clearTimeout(t._preloadTimeout), t._preloadTimeout = setTimeout(function() {
                            t.preloadNearbyImages(), t._preloadTimeout = null
                        }, 16)
                    }), void x(c + n, function() {
                        o.off(n), t.wrap.off("click" + n), t.arrowLeft && a && t.arrowLeft.add(t.arrowRight).destroyMfpFastClick(), t.arrowRight = t.arrowLeft = null
                    })) : !1
                },
                next: function() {
                    t.direction = !0, t.index = Y(t.index + 1), t.updateItemHTML()
                },
                prev: function() {
                    t.direction = !1, t.index = Y(t.index - 1), t.updateItemHTML()
                },
                goTo: function(e) {
                    t.direction = e >= t.index, t.index = e, t.updateItemHTML()
                },
                preloadNearbyImages: function() {
                    var e, i = t.st.gallery.preload,
                        n = Math.min(i[0], t.items.length),
                        o = Math.min(i[1], t.items.length);
                    for (e = 1; e <= (t.direction ? o : n); e++) t._preloadItem(t.index + e);
                    for (e = 1; e <= (t.direction ? n : o); e++) t._preloadItem(t.index - e)
                },
                _preloadItem: function(i) {
                    if (i = Y(i), !t.items[i].preloaded) {
                        var n = t.items[i];
                        n.parsed || (n = t.parseEl(i)), I("LazyLoad", n), "image" === n.type && (n.img = e('<img class="mfp-img" />').on("load.mfploader", function() {
                            n.hasSize = !0
                        }).on("error.mfploader", function() {
                            n.hasSize = !0, n.loadError = !0, I("LazyLoadError", n)
                        }).attr("src", n.src)), n.preloaded = !0
                    }
                }
            }
        });
        var q = "retina";
        e.magnificPopup.registerModule(q, {
                options: {
                    replaceSrc: function(e) {
                        return e.src.replace(/\.\w+$/, function(e) {
                            return "@2x" + e
                        })
                    },
                    ratio: 1
                },
                proto: {
                    initRetina: function() {
                        if (window.devicePixelRatio > 1) {
                            var e = t.st.retina,
                                i = e.ratio;
                            i = isNaN(i) ? i() : i, i > 1 && (x("ImageHasSize." + q, function(e, t) {
                                t.img.css({
                                    "max-width": t.img[0].naturalWidth / i,
                                    width: "100%"
                                })
                            }), x("ElementParse." + q, function(t, n) {
                                n.src = e.replaceSrc(n, i)
                            }))
                        }
                    }
                }
            }),
            function() {
                var t = 1e3,
                    i = "ontouchstart" in window,
                    n = function() {
                        k.off("touchmove" + a + " touchend" + a)
                    },
                    o = "mfpFastClick",
                    a = "." + o;
                e.fn.mfpFastClick = function(o) {
                    return e(this).each(function() {
                        var r, s = e(this);
                        if (i) {
                            var c, l, d, p, u, f;
                            s.on("touchstart" + a, function(e) {
                                p = !1, f = 1, u = e.originalEvent ? e.originalEvent.touches[0] : e.touches[0], l = u.clientX, d = u.clientY, k.on("touchmove" + a, function(e) {
                                    u = e.originalEvent ? e.originalEvent.touches : e.touches, f = u.length, u = u[0], (Math.abs(u.clientX - l) > 10 || Math.abs(u.clientY - d) > 10) && (p = !0, n())
                                }).on("touchend" + a, function(e) {
                                    n(), p || f > 1 || (r = !0, e.preventDefault(), clearTimeout(c), c = setTimeout(function() {
                                        r = !1
                                    }, t), o())
                                })
                            })
                        }
                        s.on("click" + a, function() {
                            r || o()
                        })
                    })
                }, e.fn.destroyMfpFastClick = function() {
                    e(this).off("touchstart" + a + " click" + a), i && k.off("touchmove" + a + " touchend" + a)
                }
            }(), S()
    });
var deviceIsAndroid = navigator.userAgent.indexOf("Android") > 0,
    deviceIsIOS = /iP(ad|hone|od)/.test(navigator.userAgent),
    deviceIsIOS4 = deviceIsIOS && /OS 4_\d(_\d)?/.test(navigator.userAgent),
    deviceIsIOSWithBadTarget = deviceIsIOS && /OS ([6-9]|\d{2})_\d/.test(navigator.userAgent),
    deviceIsBlackBerry10 = navigator.userAgent.indexOf("BB10") > 0;
FastClick.prototype.needsClick = function(e) {
        "use strict";
        switch (e.nodeName.toLowerCase()) {
            case "button":
            case "select":
            case "textarea":
                if (e.disabled) return !0;
                break;
            case "input":
                if (deviceIsIOS && "file" === e.type || e.disabled) return !0;
                break;
            case "label":
            case "video":
                return !0
        }
        return /\bneedsclick\b/.test(e.className)
    }, FastClick.prototype.needsFocus = function(e) {
        "use strict";
        switch (e.nodeName.toLowerCase()) {
            case "textarea":
                return !0;
            case "select":
                return !deviceIsAndroid;
            case "input":
                switch (e.type) {
                    case "button":
                    case "checkbox":
                    case "file":
                    case "image":
                    case "radio":
                    case "submit":
                        return !1
                }
                return !e.disabled && !e.readOnly;
            default:
                return /\bneedsfocus\b/.test(e.className)
        }
    }, FastClick.prototype.sendClick = function(e, t) {
        "use strict";
        var i, n;
        document.activeElement && document.activeElement !== e && document.activeElement.blur(), n = t.changedTouches[0], i = document.createEvent("MouseEvents"), i.initMouseEvent(this.determineEventType(e), !0, !0, window, 1, n.screenX, n.screenY, n.clientX, n.clientY, !1, !1, !1, !1, 0, null), i.forwardedTouchEvent = !0, e.dispatchEvent(i)
    }, FastClick.prototype.determineEventType = function(e) {
        "use strict";
        return deviceIsAndroid && "select" === e.tagName.toLowerCase() ? "mousedown" : "click"
    }, FastClick.prototype.focus = function(e) {
        "use strict";
        var t;
        deviceIsIOS && e.setSelectionRange && 0 !== e.type.indexOf("date") && "time" !== e.type ? (t = e.value.length, e.setSelectionRange(t, t)) : e.focus()
    }, FastClick.prototype.updateScrollParent = function(e) {
        "use strict";
        var t, i;
        if (t = e.fastClickScrollParent, !t || !t.contains(e)) {
            i = e;
            do {
                if (i.scrollHeight > i.offsetHeight) {
                    t = i, e.fastClickScrollParent = i;
                    break
                }
                i = i.parentElement
            } while (i)
        }
        t && (t.fastClickLastScrollTop = t.scrollTop)
    }, FastClick.prototype.getTargetElementFromEventTarget = function(e) {
        "use strict";
        return e.nodeType === Node.TEXT_NODE ? e.parentNode : e
    }, FastClick.prototype.onTouchStart = function(e) {
        "use strict";
        var t, i, n;
        if (e.targetTouches.length > 1) return !0;
        if (t = this.getTargetElementFromEventTarget(e.target), i = e.targetTouches[0], deviceIsIOS) {
            if (n = window.getSelection(), n.rangeCount && !n.isCollapsed) return !0;
            if (!deviceIsIOS4) {
                if (i.identifier && i.identifier === this.lastTouchIdentifier) return e.preventDefault(), !1;
                this.lastTouchIdentifier = i.identifier, this.updateScrollParent(t)
            }
        }
        return this.trackingClick = !0, this.trackingClickStart = e.timeStamp, this.targetElement = t, this.touchStartX = i.pageX, this.touchStartY = i.pageY, e.timeStamp - this.lastClickTime < this.tapDelay && e.preventDefault(), !0
    }, FastClick.prototype.touchHasMoved = function(e) {
        "use strict";
        var t = e.changedTouches[0],
            i = this.touchBoundary;
        return Math.abs(t.pageX - this.touchStartX) > i || Math.abs(t.pageY - this.touchStartY) > i ? !0 : !1
    }, FastClick.prototype.onTouchMove = function(e) {
        "use strict";
        return this.trackingClick ? ((this.targetElement !== this.getTargetElementFromEventTarget(e.target) || this.touchHasMoved(e)) && (this.trackingClick = !1, this.targetElement = null), !0) : !0
    }, FastClick.prototype.findControl = function(e) {
        "use strict";
        return void 0 !== e.control ? e.control : e.htmlFor ? document.getElementById(e.htmlFor) : e.querySelector("button, input:not([type=hidden]), keygen, meter, output, progress, select, textarea")
    }, FastClick.prototype.onTouchEnd = function(e) {
        "use strict";
        var t, i, n, o, a, r = this.targetElement;
        if (!this.trackingClick) return !0;
        if (e.timeStamp - this.lastClickTime < this.tapDelay) return this.cancelNextClick = !0, !0;
        if (this.cancelNextClick = !1, this.lastClickTime = e.timeStamp, i = this.trackingClickStart, this.trackingClick = !1, this.trackingClickStart = 0, deviceIsIOSWithBadTarget && (a = e.changedTouches[0], r = document.elementFromPoint(a.pageX - window.pageXOffset, a.pageY - window.pageYOffset) || r, r.fastClickScrollParent = this.targetElement.fastClickScrollParent), n = r.tagName.toLowerCase(), "label" === n) {
            if (t = this.findControl(r)) {
                if (this.focus(r), deviceIsAndroid) return !1;
                r = t
            }
        } else if (this.needsFocus(r)) return e.timeStamp - i > 100 || deviceIsIOS && window.top !== window && "input" === n ? (this.targetElement = null, !1) : (this.focus(r), this.sendClick(r, e), deviceIsIOS && "select" === n || (this.targetElement = null, e.preventDefault()), !1);
        return deviceIsIOS && !deviceIsIOS4 && (o = r.fastClickScrollParent, o && o.fastClickLastScrollTop !== o.scrollTop) ? !0 : (this.needsClick(r) || (e.preventDefault(), this.sendClick(r, e)), !1)
    }, FastClick.prototype.onTouchCancel = function() {
        "use strict";
        this.trackingClick = !1, this.targetElement = null
    }, FastClick.prototype.onMouse = function(e) {
        "use strict";
        return this.targetElement ? e.forwardedTouchEvent ? !0 : e.cancelable && (!this.needsClick(this.targetElement) || this.cancelNextClick) ? (e.stopImmediatePropagation ? e.stopImmediatePropagation() : e.propagationStopped = !0, e.stopPropagation(), e.preventDefault(), !1) : !0 : !0
    }, FastClick.prototype.onClick = function(e) {
        "use strict";
        var t;
        return this.trackingClick ? (this.targetElement = null, this.trackingClick = !1, !0) : "submit" === e.target.type && 0 === e.detail ? !0 : (t = this.onMouse(e), t || (this.targetElement = null), t)
    }, FastClick.prototype.destroy = function() {
        "use strict";
        var e = this.layer;
        deviceIsAndroid && (e.removeEventListener("mouseover", this.onMouse, !0), e.removeEventListener("mousedown", this.onMouse, !0), e.removeEventListener("mouseup", this.onMouse, !0)), e.removeEventListener("click", this.onClick, !0), e.removeEventListener("touchstart", this.onTouchStart, !1), e.removeEventListener("touchmove", this.onTouchMove, !1), e.removeEventListener("touchend", this.onTouchEnd, !1), e.removeEventListener("touchcancel", this.onTouchCancel, !1)
    }, FastClick.notNeeded = function(e) {
        "use strict";
        var t, i, n;
        if ("undefined" == typeof window.ontouchstart) return !0;
        if (i = +(/Chrome\/([0-9]+)/.exec(navigator.userAgent) || [, 0])[1]) {
            if (!deviceIsAndroid) return !0;
            if (t = document.querySelector("meta[name=viewport]")) {
                if (-1 !== t.content.indexOf("user-scalable=no")) return !0;
                if (i > 31 && document.documentElement.scrollWidth <= window.outerWidth) return !0
            }
        }
        if (deviceIsBlackBerry10 && (n = navigator.userAgent.match(/Version\/([0-9]*)\.([0-9]*)/), n[1] >= 10 && n[2] >= 3 && (t = document.querySelector("meta[name=viewport]")))) {
            if (-1 !== t.content.indexOf("user-scalable=no")) return !0;
            if (document.documentElement.scrollWidth <= window.outerWidth) return !0
        }
        return "none" === e.style.msTouchAction ? !0 : !1
    }, FastClick.attach = function(e, t) {
        "use strict";
        return new FastClick(e, t)
    }, "function" == typeof define && "object" == typeof define.amd && define.amd ? define(function() {
        "use strict";
        return FastClick
    }) : "undefined" != typeof module && module.exports ? (module.exports = FastClick.attach, module.exports.FastClick = FastClick) : window.FastClick = FastClick, ! function(e, t, i, n) {
        "use strict";

        function o(e) {
            var t, i, n, o, a, r, s, c = {};
            for (a = e.replace(/\s*:\s*/g, ":").replace(/\s*,\s*/g, ",").split(","), s = 0, r = a.length; r > s && (i = a[s], -1 === i.search(/^(http|https|ftp):\/\//) && -1 !== i.search(":")); s++) t = i.indexOf(":"), n = i.substring(0, t), o = i.substring(t + 1), o || (o = void 0), "string" == typeof o && (o = "true" === o || ("false" === o ? !1 : o)), "string" == typeof o && (o = isNaN(o) ? o : +o), c[n] = o;
            return null == n && null == o ? e : c
        }

        function a(e) {
            e = "" + e;
            var t, i, n, o = e.split(/\s+/),
                a = "50%",
                r = "50%";
            for (n = 0, t = o.length; t > n; n++) i = o[n], "left" === i ? a = "0%" : "right" === i ? a = "100%" : "top" === i ? r = "0%" : "bottom" === i ? r = "100%" : "center" === i ? 0 === n ? a = "50%" : r = "50%" : 0 === n ? a = i : r = i;
            return {
                x: a,
                y: r
            }
        }

        function r(t, i) {
            var n = function() {
                i(this.src)
            };
            e("<img src='" + t + ".gif'>").load(n), e("<img src='" + t + ".jpg'>").load(n), e("<img src='" + t + ".jpeg'>").load(n), e("<img src='" + t + ".png'>").load(n)
        }

        function s(t, i, n) {
            if (this.$element = e(t), "string" == typeof i && (i = o(i)), n ? "string" == typeof n && (n = o(n)) : n = {}, "string" == typeof i) i = i.replace(/\.\w*$/, "");
            else if ("object" == typeof i)
                for (var a in i) i.hasOwnProperty(a) && (i[a] = i[a].replace(/\.\w*$/, ""));
            this.settings = e.extend({}, l, n), this.path = i, this.init()
        }
        var c = "vide",
            l = {
                volume: 1,
                playbackRate: 1,
                muted: !0,
                loop: !0,
                autoplay: !0,
                position: "50% 50%",
                posterType: "detect",
                resizing: !0
            },
            d = /iPad|iPhone|iPod/i.test(n.userAgent),
            p = /Android/i.test(n.userAgent);
        s.prototype.init = function() {
            var t, i, n = this,
                o = a(n.settings.position);
            n.$wrapper = e("<div>").css({
                position: "absolute",
                "z-index": -1,
                top: 0,
                left: 0,
                bottom: 0,
                right: 0,
                overflow: "hidden",
                "-webkit-background-size": "cover",
                "-moz-background-size": "cover",
                "-o-background-size": "cover",
                "background-size": "cover",
                "background-repeat": "no-repeat",
                "background-position": o.x + " " + o.y
            }), i = n.path, "object" == typeof n.path && (n.path.poster ? i = n.path.poster : n.path.mp4 ? i = n.path.mp4 : n.path.webm ? i = n.path.webm : n.path.ogv && (i = n.path.ogv)), "detect" === n.settings.posterType ? r(i, function(e) {
                n.$wrapper.css("background-image", "url(" + e + ")")
            }) : "none" !== n.settings.posterType && n.$wrapper.css("background-image", "url(" + i + "." + n.settings.posterType + ")"), "static" === n.$element.css("position") && n.$element.css("position", "relative"), n.$element.prepend(n.$wrapper), d || p || (t = "", "object" == typeof n.path ? (n.path.mp4 && (t += "<source src='" + n.path.mp4 + ".mp4' type='video/mp4'>"), n.path.webm && (t += "<source src='" + n.path.webm + ".webm' type='video/webm'>"), n.path.ogv && (t += "<source src='" + n.path.ogv + ".ogv' type='video/ogv'>"), n.$video = e("<video>" + t + "</video>")) : n.$video = e("<video><source src='" + n.path + ".mp4' type='video/mp4'><source src='" + n.path + ".webm' type='video/webm'><source src='" + n.path + ".ogv' type='video/ogg'></video>"), n.$video.css("visibility", "hidden"), n.$video.prop({
                autoplay: n.settings.autoplay,
                loop: n.settings.loop,
                volume: n.settings.volume,
                muted: n.settings.muted,
                playbackRate: n.settings.playbackRate
            }), n.$wrapper.append(n.$video), n.$video.css({
                margin: "auto",
                position: "absolute",
                "z-index": -1,
                top: o.y,
                left: o.x,
                "-webkit-transform": "translate(-" + o.x + ", -" + o.y + ")",
                "-ms-transform": "translate(-" + o.x + ", -" + o.y + ")",
                transform: "translate(-" + o.x + ", -" + o.y + ")"
            }), n.$video.bind("loadedmetadata." + c, function() {
                n.$video.css("visibility", "visible"), n.resize(), n.$wrapper.css("background-image", "none")
            }), n.$element.bind("resize." + c, function() {
                n.settings.resizing && n.resize()
            }))
        }, s.prototype.getVideoObject = function() {
            return this.$video ? this.$video[0] : null
        }, s.prototype.resize = function() {
            if (this.$video) {
                var e = this.$video[0].videoHeight,
                    t = this.$video[0].videoWidth,
                    i = this.$wrapper.height(),
                    n = this.$wrapper.width();
                this.$video.css(n / t > i / e ? {
                    width: n + 2,
                    height: "auto"
                } : {
                    width: "auto",
                    height: i + 2
                })
            }
        }, s.prototype.destroy = function() {
            this.$element.unbind(c), this.$video && this.$video.unbind(c), delete e[c].lookup[this.index], this.$element.removeData(c), this.$wrapper.remove()
        }, e[c] = {
            lookup: []
        }, e.fn[c] = function(t, i) {
            var n;
            return this.each(function() {
                n = e.data(this, c), n && n.destroy(), n = new s(this, t, i), n.index = e[c].lookup.push(n) - 1, e.data(this, c, n)
            }), this
        }, e(i).ready(function() {
            e(t).bind("resize." + c, function() {
                for (var t, i = e[c].lookup.length, n = 0; i > n; n++) t = e[c].lookup[n], t && t.settings.resizing && t.resize()
            }), e(i).find("[data-" + c + "-bg]").each(function(t, i) {
                var n = e(i),
                    o = n.data(c + "-options"),
                    a = n.data(c + "-bg");
                n[c](a, o)
            })
        })
    }(window.jQuery, window, document, navigator),
    function() {
        function e() {
            n && clearTimeout(n), o && clearTimeout(o), n = setTimeout(function() {
                var e = $(".onepage-pagination").find("a.active").data("index");
                2 == e ? (o && clearTimeout(o), o = setTimeout(function() {
                    $(".logo").addClass("show")
                }, 400)) : $(".logo").removeClass("show")
            }, 50)
        }

        function t(e) {
            e.show().animate({
                opacity: 1
            })
        }

        function i() {
            c.children().hide().css("opacity", 0)
        }
        FastClick.attach(document.body), $(".page_slider").onepage_scroll({
            sectionContainer: ".slide",
            easing: "ease",
            animationTime: 500,
            pagination: !0,
            updateURL: !1,
            responsiveFallback: 768,
            loop: !1
        }), $(".down_arrow").on("click", function() {
            $(".page_slider").moveDown()
        });
        var n, o, a = $("#video-container");
        a.vide("/images/splash/bg/bg", {
            volume: 0,
            playbackRate: 1,
            muted: !0,
            loop: !0,
            autoplay: !0,
            position: "65% 0%",
            posterType: "jpg"
        });
        var r = a.data("vide"),
            s = r.getVideoObject();
        $(document).on("mousewheel DOMMouseScroll click touchend", e), $(".video-lightbox").magnificPopup({
            type: "iframe",
            mainClass: "mfp-fade",
            removalDelay: 160,
            preloader: !1,
            fixedContentPos: !1,
            callbacks: {
                open: function() {
                    ga("send", "event", "video", "click"), s && s.pause()
                },
                close: function() {
                    s && s.play()
                }
            }
        });
        var c = $("#waitlist-form-message-container");
        $(".appstore-download-link").click(function() {
            ga("send", "event", "download", "click", "ios")
        });
        var l = $("#waitlist-form"),
            d = l.find(".email"),
            p = l.find(".submit");
        d.focus(i), l.submit(function(e) {
            e.preventDefault();
            var n = $.trim(d.val());
            return i(), n ? (p.attr("disabled", !0), void $.post("https://api.completeapp.com/api/waitlist", {
                email: n
            }).success(function() {
                p.attr("disabled", !1), t(c.find(".waitlist-form-message--success"))
            }).error(function(e) {
                p.attr("disabled", !1), e = $.parseJSON(e.responseText);
                var i, n = e.error || (e.errors && e.errors.length ? e.errors[0] : null),
                    o = n.code;
                switch (o) {
                    case "already_in":
                        i = c.find(".waitlist-form-message--already-in");
                        break;
                    default:
                        i = c.find(".waitlist-form-message--error")
                }
                t(i)
            })) : void d.addClass("invalid")
        })
    }();